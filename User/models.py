from django.db import models
from PIL import Image
from django.contrib.auth.models import User
from phonenumber_field.modelfields import PhoneNumberField

STATE_CHOICES = (
    ('Andaman & Nicobar Islands', 'Andaman & Nicobar Islands'),
    ('Andhra Pradesh', 'Andhra Pradesh'),
    ('Gujarat', 'Gujarat'),
    ('Bihar', 'Bihar'),
    ('Daman & Diu', 'Daman & Diu'),
    ('Haryana', 'Haryana'),
    ('Uttar Pradesh', 'Uttar Pradesh'),
    ('West Bengal', 'West Bengal'),
)


# Customer Profile Table
class Customer(models.Model):
    """
    Customer Details Table
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email = models.CharField(max_length=200, null=True)
    firstname = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200, null=True)
    mobile_no = PhoneNumberField()
    city = models.CharField(max_length=50)
    state = models.CharField(choices=STATE_CHOICES, max_length=50)
    Zip = models.IntegerField(default=0)
    Address = models.CharField(max_length=200)
    profile_pic = models.ImageField(default='default.jpg', upload_to='profile_pics')

    def __str__(self):
        return str(self.user)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        img = Image.open(self.profile_pic.path)
        if img.height > 300 and img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.profile_pic.path)


class Addres(models.Model):
    """
    Customer Details Table
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    firstname = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200, null=True)
    email = models.CharField(max_length=200, null=True)
    mobile_no = PhoneNumberField()
    city = models.CharField(max_length=50)
    state = models.CharField(choices=STATE_CHOICES, max_length=50)
    Zip = models.IntegerField(default=0)
    residential_address = models.TextField()

    def __str__(self):
        return str(self.user)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)