from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Customer, Addres


class CustomerRegistration(UserCreationForm):
    """
    Customer registration Form
    """

    class Meta:
        model = User
        fields = ['username', 'password1',  'password2']


class UserUpdateForm(forms.ModelForm):
    """
    Customer Profile Update Form
    """

    class Meta:
        model = Customer
        fields = ['firstname', 'last_name', 'email', 'mobile_no', 'city', 'state', 'Zip', 'Address', 'profile_pic']


class UserAddress(forms.ModelForm):
    class Meta:
        """
        Multiple UserAddress Update form
        """
        model = Addres
        fields = ['firstname', 'last_name', 'email', 'mobile_no', 'city', 'state', 'Zip', 'residential_address']
