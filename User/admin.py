from django.contrib import admin
from .models import Customer, Addres

admin.site.register(Customer)


@admin.register(Addres)
class AddressModelAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'firstname', 'last_name', 'email', 'mobile_no', 'city', 'state', 'Zip',
                    'residential_address']
