from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.views import View
from django.contrib import messages
from .models import Addres
from django.contrib.auth.models import User
from .forms import CustomerRegistration, UserUpdateForm, UserAddress
from store.models import Category


class CustomerRegister(View):
    def get(self, request):
        """
        Get Request for Customer Registration
        """
        form = CustomerRegistration
        context = {'form': form}
        return render(request, 'User/register.html', context)

    def post(self, request):
        """
        Post Request for Customer Forms
        """
        email = request.POST.get('email')
        if User.objects.filter(email=email).exists():
            messages.warning(request, 'Email id already Exists !')
            return redirect('register')
        else:
            form = CustomerRegistration(request.POST)
            if form.is_valid():
                form.save()
                messages.success(request, 'Thank you for Registration! Welcome to E-Grocery Family')
                return redirect('login')
            return render(request, 'User/register.html', {'form': form})


class ProfileView(View):
    def get(self, request):
        """
        Profile View for Authenticated Users
        """
        category = Category.objects.all()
        user_form = UserUpdateForm(instance=request.user.customer)
        return render(request, 'User/profile.html', {'user_form': user_form, 'category': category})

    def post(self, request):
        """
        Profile View for Updation for Profile
        """
        user_form = UserUpdateForm(request.POST, request.FILES, instance=request.user.customer)
        if user_form.is_valid():
            user_form.save()
            messages.success(request, f'Profile is Updated')
            return redirect('profile')
        return render(request, 'User/profile.html', {'user_form': user_form})


class NewAddressView(View):
    def get(self, request):
        """
        Fetch the data from database
        """
        category = Category.objects.all()
        address_form = UserAddress()
        return render(request, 'User/add_new_address.html', {'address_form': address_form, 'category': category})

    def post(self, request):
        """
        New Address Form
        """
        address_form = UserAddress(request.POST)
        if address_form.is_valid():
            firstname = address_form.cleaned_data['firstname']
            last_name = address_form.cleaned_data['last_name']
            email = address_form.cleaned_data['email']
            mobile_no = address_form.cleaned_data['mobile_no']
            city = address_form.cleaned_data['city']
            state = address_form.cleaned_data['state']
            Zip = address_form.cleaned_data['Zip']
            Address = address_form.cleaned_data['residential_address']
            new_add = Addres(user=request.user, firstname=firstname, last_name=last_name, email=email,
                             mobile_no=mobile_no, city=city, state=state, Zip=Zip, residential_address=Address)
            new_add.save()
            messages.success(request, f'New Address Added')
            return redirect('address')
        return render(request, 'User/add_new_address.html', {'address_form': address_form})


class EditAddressView(View):
    def get(self, request, pk):
        """
        View for Editing address
        """
        address = Addres.objects.get(pk=pk, user=request.user)
        address_form = UserAddress(instance=address)
        return render(request, 'User/add_new_address.html', {'address_form': address_form})

    def post(self, request, pk):
        """
        View for Editing Address
        """
        category = Category.objects.all()
        address = Addres.objects.get(pk=pk, user=request.user)
        address_form = UserAddress(request.POST, instance=address)
        if address_form.is_valid():
            address_form.save()
            messages.success(request, f'Address Updated Successfully !!! ')
            return redirect('address')
        return render(request, 'User/addresses.html', {'address_form': address_form, 'category': category})


class AddressDeleteView(View):
    def post(self, request):
        """
        Method for deleting saved addresses
        """
        address_id = request.POST['address_id']
        Addres(id=address_id).delete()
        return JsonResponse({'status': 'Save'})


class AddressView(View):
    def get(self, request):
        """
         User's Saved Address View
        """
        address = Addres.objects.filter(user=request.user)
        category = Category.objects.all()
        return render(request, 'User/addresses.html', {'address': address, 'category':category})
