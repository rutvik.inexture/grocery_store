from django.contrib.auth.decorators import login_required
from django.urls import path
from User import views
from django.contrib.auth import views as auth_views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('register/', views.CustomerRegister.as_view(), name='register'),
    path('profile/', login_required(views.ProfileView.as_view()), name='profile'),
    path('address/', login_required(views.AddressView.as_view()), name='address'),
    path('addnewaddress/', login_required(views.NewAddressView.as_view()), name='add_new_address'),
    path('editaddress/<int:pk>', login_required(views.EditAddressView.as_view()), name='edit_address'),
    path('addressdelete/', login_required(views.AddressDeleteView.as_view()), name='del_address'),
    path('login/', auth_views.LoginView.as_view(template_name='User/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='User/logout.html'), name='logout'),
    path('password-change/', auth_views.PasswordChangeView.as_view(template_name='User/password_change.html'),
         name='changepassword'),
    path('password-change/done/',
         auth_views.PasswordChangeDoneView.as_view(template_name='User/password_change_done.html'),
         name='password_change_done'),
    path('password-reset/', auth_views.PasswordResetView.as_view(template_name='User/password_reset.html'),
         name='password_reset'),
    path('password-reset-confirm/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(template_name='User/password_reset_confirm.html'),
         name='password_reset_confirm'),
    path('password-reset/done/',
         auth_views.PasswordResetDoneView.as_view(template_name='User/password_reset_done.html'),
         name='password_reset_done'),
    path('password-reset-complete/',
         auth_views.PasswordResetCompleteView.as_view(template_name='User/password_reset_complete.html'),
         name='password_reset_complete'),

]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
