from django import forms
from django.core.exceptions import ValidationError


# Validation for password field of django forms
class PasswordValidator:
    def validate(self, password, user=None):
        specialsym = ['$', '@', '#', '%']
        print('i am here')
        if not any(char.isdigit() for char in password):
            print("number123")
            raise ValidationError('Atleast one number is Required')
        if not any(char.isupper() for char in password):
            print("uppercase")
            raise ValidationError('Atleast one Uppercase is Required')
        if not any(char.islower() for char in password):
            print("lowercase")
            raise ValidationError('Atleast one Lowercase is Required')
        if not any(char in specialsym for char in password):
            print("specialsymbol")
            raise ValidationError('Atleast one SpecialSymbol ($#@) is Required')

    def get_help_text(self):
        return ('Atleast one Uppercase, Lowercase,Number,SpecialSymbol is Required')
