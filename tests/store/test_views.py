import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_product_view(user, client):
    url = reverse('homepage')
    response = client.get(url)
    assert 'Sale Update' in str(response.content)
    assert response.status_code == 200


@pytest.mark.django_db
def test_product_detail_view(user, client, product):
    url = reverse('product-detail', args=[product.id])
    response = client.get(url)
    assert 'Available Offers' in str(response.content)
    assert response.status_code == 200


@pytest.mark.django_db
def test_category_view(user, client, category):
    url = reverse('category', args=[category.id])
    response = client.get(url)
    assert f"All {category.category}" in str(response.content)
    assert response.status_code == 200


@pytest.mark.django_db
def test_category_data_view_low(user, client, category):
    url = reverse('category-data', args=[category.id, 'price-low-to-high'])
    response = client.get(url)
    assert f"All {category.category}" in str(response.content)
    assert response.status_code == 200


@pytest.mark.django_db
def test_category_data_view_high(user, client, category):
    url = reverse('category-data', args=[category.id, 'price-high-to-low'])
    response = client.get(url)
    assert f"All {category.category}" in str(response.content)
    assert response.status_code == 200


@pytest.mark.django_db
def test_category_data_view_popular(user, client, category):
    url = reverse('category-data', args=[category.id, 'popular-products'])
    response = client.get(url)
    assert f"All {category.category}" in str(response.content)
    assert response.status_code == 200


@pytest.mark.django_db
def test_category_data_view_filter(user, client, category):
    url = '/category-filter/1?min_value=120&minimum=15&max_value=300&maximum=500'
    response = client.get(url)
    assert f"All {category.category}" in str(response.content)
    assert response.status_code == 200



@pytest.mark.django_db
def test_add_to_cart_view(login_user, client, product):
    url = '/add-to-cart?1/'
    response = client.get(url)
    assert response.status_code == 301

@pytest.mark.django_db
def test_buy_now_view(login_user, client, product):
    url = '/buy-now?1/'
    response = client.get(url)
    assert response.status_code == 301

@pytest.mark.django_db
def test_empty_cart_get_view(login_user, client, product):
    url = reverse('cart')
    response = client.get(url)
    assert "Your Cart is Empty!! Add items" in str(response.content)
    assert response.status_code == 200


@pytest.mark.django_db
def test_cart_post_view_condition_applied(login_user, client, product, coupon_code, cart):
    data = {'coupon_code': coupon_code.code, 'total_amount': 850, 'discount': coupon_code.discount}
    url = reverse('cart')
    response = client.post(url, data=data)
    assert "'s Shopping Cart" in str(response.content)
    assert f"Cart Products" in str(response.content)
    assert response.status_code == 200


@pytest.mark.django_db
def test_cart_post_view_no_condition_applied(login_user, client, product, new_coupon_code, cart):
    data = {'coupon_code': new_coupon_code.code, 'total_amount': 850, 'discount': new_coupon_code.discount}
    url = reverse('cart')
    response = client.post(url, data=data)
    assert "'s Shopping Cart" in str(response.content)
    assert f"Cart Products" in str(response.content)
    assert response.status_code == 200



@pytest.mark.django_db
def test_update_cart_view(login_user, client, product, cart):
    data = {'product_id': product.id, 'product_qty': 1, 'amount': 800, 'total_amount': 850}
    url = reverse('updatecart')
    response = client.post(url, data=data)
    assert '{"amount": 449.0, "totalamount": 519.0}' in str(response.content)
    assert response.status_code == 200


@pytest.mark.django_db
def test_remove_cart_view(login_user, client, product, cart):
    data = {'product_id': product.id, 'shipping_amount': 100}
    url = reverse('removecart')
    response = client.post(url, data=data)
    assert '{"amount": 0.0, "totalamount": 0.0}' in str(response.content)
    assert response.status_code == 200


@pytest.mark.django_db
def test_review_view(login_user, client, product, review):
    data = {'product_id': product.id, 'review': review.review}
    url = reverse('review')
    response = client.post(url, data=data)
    assert '{"status": "Save"}' in str(response.content)
    assert response.status_code == 200


@pytest.mark.django_db
def test_review_delete_view(login_user, client, review):
    data = {'review_id': review.id}
    url = reverse('reviewdelete')
    response = client.post(url, data=data)
    assert '{"status": "Save"}' in str(response.content)
    assert response.status_code == 200


@pytest.mark.django_db
def test_search_product_view(login_user, client, product):
    url = reverse('search')
    response = client.get(url)
    assert '["Mother Dairy Cow Ghee 1 L (Pouch)"]' in str(response.content)
    assert response.status_code == 200


@pytest.mark.django_db
def test_no_search_product_item_view(login_user, client, product):
    url = reverse('searcheditem')
    response = client.get(url)
    assert "" in str(response.content)
    assert response.status_code == 302


@pytest.mark.django_db
def test_search_product_item_view_post(login_user, client, product):
    data = {'searchproducts': 'Mother Dairy Cow Ghee 1 L (Pouch)'}
    url = reverse('searcheditem')
    response = client.post(url, data=data)
    assert 'Product Name:' in str(response.content)
    assert response.status_code == 200


@pytest.mark.django_db
def test_checkout_view(login_user, client, product):
    url = reverse('checkout')
    response = client.get(url)
    assert 'Select Shipping Address' in str(response.content)
    assert response.status_code == 200


@pytest.mark.django_db
def test_order_place_view(login_user, client, product, cart, address, coupon_code):
    data = {'address_id': address.id, 'prod_id': product.id, 'prod_stock': product.stock, 'order_qty': 1,
            'code': coupon_code.code, 'total_amount': 500}
    url = reverse('Place-order')
    response = client.post(url, data=data)
    assert response.status_code == 302


@pytest.mark.django_db
def test_orders_view(login_user, client, product, cart, address, order_placed):
    url = reverse('orders')
    response = client.get(url)
    assert 'Order Status:' in str(response.content)
    assert response.status_code == 200


@pytest.mark.django_db
def test_no_orders_view(login_user, client, product, cart, address):
    url = reverse('orders')
    response = client.get(url)
    assert response.status_code == 302


@pytest.mark.django_db
def test_cancel_order_view(login_user, client, product, order_placed):
    data = {'order_id': 1, 'prod_id': product.id, 'prod_stock': product.stock, 'order_qty': 1}
    url = reverse('cancel-order')
    response = client.post(url, data=data)
    assert '{"status": "Save"}' in str(response.content)
    assert response.status_code == 200


@pytest.mark.django_db
def test_order_history_view(login_user, client, product, cart, review, order_placed):
    url = reverse('order-history')
    response = client.get(url)
    assert 'Orders History' in str(response.content)
    assert response.status_code == 200


@pytest.mark.django_db
def test_no_order_history_view(login_user, client, product, cart, review):
    url = reverse('order-history')
    response = client.get(url)
    assert response.status_code == 302


@pytest.mark.django_db
def test_create_checkout_view(login_user, client, product, coupon_code, address, cart, review, order_placed):
    data = {'address_id': address.id, 'total_amount': order_placed.total_amount, 'prod_id': product.id,
            'prod_stock': product.stock, 'order_qty': 1, 'code': coupon_code}
    url = reverse('create-checkout-session')
    response = client.post(url, data=data)
    assert response.status_code == 302


@pytest.mark.django_db
def test_cancel_checkout_view(login_user, client, product, coupon_code, address, cart, review, order_placed):
    url = '/cancelorder/?product_ids=%5B24%5D&product_stock=%5B11%5D&order_quantity=%5B1%5D&order_ids=%5B562%5D'
    response = client.get(url)
    assert response.status_code == 302


@pytest.mark.django_db
def test_customer_registration_get(client):
    url = reverse('register')
    response = client.get(url)
    assert 'Welcome to E-Grocery' in str(response.content)
    assert response.status_code == 200


@pytest.mark.django_db
def test_customer_registration_post_email_already_exists(client, user):
    data = {'email': user.email}
    url = reverse('register')
    response = client.post(url, data=data)
    assert response.status_code == 302


@pytest.mark.django_db
def test_profile_view(login_user, client):
    url = reverse('profile')
    response = client.get(url)
    assert 'Profile Info' in str(response.content)
    assert response.status_code == 200


@pytest.mark.django_db
def test_address_view(login_user, client, user):
    url = reverse('address')
    response = client.get(url)
    assert 'Welcome' in str(response.content)
    assert response.status_code == 200


@pytest.mark.django_db
def test_address_delete_view(login_user, client, user, address):
    data = {'address_id': address.id}
    url = reverse('del_address')
    response = client.post(url, data=data)
    assert '{"status": "Save"}' in str(response.content)
    assert response.status_code == 200


@pytest.mark.django_db
def test_edit_address_view_get(login_user, client, user, address):
    url = reverse('edit_address', args=[address.id])
    response = client.get(url)
    assert 'Add New Address' in str(response.content)
    assert response.status_code == 200

