import datetime

import pytest


@pytest.mark.django_db
def test_category_model(category):
    assert category.category == "Beverages"


@pytest.mark.django_db
def test_product_model(product, category):
    assert product.title == "Mother Dairy Cow Ghee 1 L (Pouch)"
    assert product.selling_price == 550
    assert product.discounted_price == 449
    assert product.description == "Mother Dairy Cow Ghee 1 L (Pouch) Ghee is a class of clarified butter that originated in ancient India. It is commonly used in Indian cooking. Mother Dairy Cow Ghee can be swapped for vegetable oil or coconut oil in baked goods or can be used for deep-frying. Or simply melt it and spread it on roti, or pour it on vegetables/dal before serving. So go ahead and buy this product online today!"
    assert product.stock == 10
    assert product.category == "Dairy"


@pytest.mark.django_db
def test_cart_model(cart, user, product):
    assert cart.user == user
    assert cart.product == product
    assert cart.quantity == 1
    assert cart.ordered == False


@pytest.mark.django_db
def test_review_model(review, user, product):
    assert review.user == user
    assert review.product == product
    assert review.review == 'Nice'


@pytest.mark.django_db
def test_order_placed_model(order_placed, user, product, address):
    assert order_placed.user == user
    assert order_placed.product == product
    assert order_placed.quantity == 1
    assert order_placed.address == address
    assert order_placed.ordered_date == datetime.date(2022, 7, 21)
    assert order_placed.mode == 'online'
    assert order_placed.status == 'Pending'
    assert order_placed.is_canceled == False
    assert order_placed.total_amount == 220.0


@pytest.mark.django_db
def test_coupon_code_model(coupon_code):
    assert coupon_code.code == 'FLAT5'
    assert coupon_code.valid_from == '2022-07-18 06:00:00'
    assert coupon_code.valid_to == '2022-07-28 06:00:00'
    assert coupon_code.discount == 5
    assert coupon_code.active == True
    assert coupon_code.condition == '500-1000'


@pytest.mark.django_db
def test_applied_coupon_model(applied_coupon, user, coupon_code):
    assert applied_coupon.coupon == coupon_code
    assert applied_coupon.user == user
