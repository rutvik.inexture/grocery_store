import pytest
from django.contrib.auth.views import *
from django.urls import reverse, resolve
from store.views import *
from User.views import *


def test_home_url():
    url = reverse('homepage')
    assert resolve(url).func.view_class == ProductView


def test_product_detail_url():
    url = reverse('product-detail', args=[1])
    assert resolve(url).func.view_class == ProductDetailView


def test_cart_url():
    url = reverse('cart')
    assert resolve(url).func.view_class == CartView


def test_product_list_search_url():
    url = reverse('search')
    assert resolve(url).func.view_class == SearchProductView


def test_searched_items_url():
    url = reverse('searcheditem')
    assert resolve(url).func.view_class == SearchedItemView


def test_add_to_cart_url():
    url = reverse('addtocart')
    assert resolve(url).func.view_class == AddToCartView


def test_buy_now_url():
    url = reverse('buy-now')
    assert resolve(url).func.view_class == BuyNowView


def test_add_review_url():
    url = reverse('review')
    assert resolve(url).func.view_class == ReviewView


def test_review_delete_url():
    url = reverse('reviewdelete')
    assert resolve(url).func.view_class == ReviewDeleteView

def test_update_cart_url():
    url = reverse('updatecart')
    assert resolve(url).func.view_class == UpdateCartView

def test_remove_cart_url():
    url = reverse('removecart')
    assert resolve(url).func.view_class == RemoveCartView


def test_category_url():
    url = reverse('category', args=[1])
    assert resolve(url).func.view_class == CategoryView


def test_category_filter_url():
    url = reverse('category-filter', args=[1])
    assert resolve(url).func.view_class == FilterCategoryView


def test_category_data_url():
    url = reverse('category-data', args=[1, 'popular_products'])
    assert resolve(url).func.view_class == CategoryView


def test_checkout_url():
    url = reverse('checkout')
    assert resolve(url).func.view_class == CheckoutView


def test_place_order_url():
    url = reverse('Place-order')
    assert resolve(url).func.view_class == OrderPlacedView


def test_orders_url():
    url = reverse('orders')
    assert resolve(url).func.view_class == OrdersView


def test_order_history_url():
    url = reverse('order-history')
    assert resolve(url).func.view_class == OrderHistoryView


def test_cancel_order_url():
    url = reverse('cancel-order')
    assert resolve(url).func.view_class == CancelOrderView


def test_create_checkout_session_url():
    url = reverse('create-checkout-session')
    assert resolve(url).func.view_class == CreateCheckoutSessionView


def test_cancel_checkout_session_url():
    url = reverse('cancelorder')
    assert resolve(url).func.view_class == CancelOrderCheckoutView


def test_register_url():
    url = reverse('register')
    assert resolve(url).func.view_class == CustomerRegister


def test_profile_url():
    url = reverse('profile')
    assert resolve(url).func.view_class == ProfileView


def test_address_url():
    url = reverse('address')
    assert resolve(url).func.view_class == AddressView


def test_add_address_url():
    url = reverse('add_new_address')
    assert resolve(url).func.view_class == NewAddressView


def test_edit_address():
    url = reverse('edit_address', args=[1])
    assert resolve(url).func.view_class == EditAddressView


def test_address_delete_url():
    url = reverse('del_address')
    assert resolve(url).func.view_class == AddressDeleteView


def test_login_url():
    url = reverse('login')
    assert resolve(url).func.view_class == LoginView


def test_logout_url():
    url = reverse('logout')
    assert resolve(url).func.view_class == LogoutView


def test_password_change_url():
    url = reverse('changepassword')
    assert resolve(url).func.view_class == PasswordChangeView


def test_password_change_done_url():
    url = reverse('password_change_done')
    assert resolve(url).func.view_class == PasswordChangeDoneView


def test_password_reset_url():
    url = reverse('password_reset')
    assert resolve(url).func.view_class == PasswordResetView


def test_password_reset_confirm_url():
    url = reverse('password_reset_confirm', args=['uidb64','<token>'])
    assert resolve(url).func.view_class == PasswordResetConfirmView


def test_password_reset_done_url():
    url = reverse('password_reset_done')
    assert resolve(url).func.view_class == PasswordResetDoneView


def test_password_reset_confirm_done_url():
    url = reverse('password_reset_complete')
    assert resolve(url).func.view_class == PasswordResetCompleteView







