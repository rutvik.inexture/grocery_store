import pytest
from django.test.client import Client

from User.models import *
from store.models import Product, Category, CouponCode, Cart, Review, OrderPlaced, AppliedCouponCode


@pytest.fixture
def user():
    new_user = User.objects.create_user(
        username="rutvik_9",
        email='rutvik123@gmail.com',
    )
    new_user.set_password("Rutvik@123")
    new_user.save()
    return new_user


@pytest.fixture
def new_user():
    new_users = User.objects.create_user(
        id=6,
        username="Aaa_9",
        email='abc123@gmail.com',
    )
    new_users.set_password("Abc@123")
    new_users.save()
    return new_users


@pytest.fixture
def client():
    return Client()


@pytest.fixture
def login_user(user, client):
    response = client.post('/login/', dict(username=user.username, password='Rutvik@123'))
    return response


@pytest.fixture
def product():
    products = Product.objects.create(
        id=1,
        title='Mother Dairy Cow Ghee 1 L (Pouch)',
        selling_price=550,
        discounted_price=449,
        description='Mother Dairy Cow Ghee 1 L (Pouch) Ghee is a class of clarified butter that originated in ancient India. It is commonly used in Indian cooking. Mother Dairy Cow Ghee can be swapped for vegetable oil or coconut oil in baked goods or can be used for deep-frying. Or simply melt it and spread it on roti, or pour it on vegetables/dal before serving. So go ahead and buy this product online today!',
        stock=10,
        category='Dairy',
    )
    products.save()
    return products


@pytest.fixture
def category():
    categories = Category.objects.create(
        id=1,
        category='Beverages',
    )
    categories.save()
    return categories


@pytest.fixture
def cart(user, product):
    carts = Cart.objects.create(
        user=user,
        product=product,
        quantity=1,
        ordered=False,
    )
    carts.save()
    return carts


@pytest.fixture
def coupon_code():
    code = CouponCode.objects.create(
        code='FLAT5',
        valid_from='2022-07-18 06:00:00',
        valid_to='2022-07-28 06:00:00',
        discount=5,
        active=True,
        condition='500-1000',
    )
    code.save()
    return code


@pytest.fixture
def new_coupon_code():
    code1 = CouponCode.objects.create(
        code='FLAT10',
        valid_from='2022-07-18 06:00:00',
        valid_to='2022-07-28 06:00:00',
        discount=6,
        active=True,
        condition='None',
    )
    code1.save()
    return code1


@pytest.fixture
def review(user, product):
    reviews = Review.objects.create(
        user=user,
        product=product,
        review='Nice',
    )
    reviews.save()
    return reviews


@pytest.fixture
def address(user):
    addresses = Addres.objects.create(
        user=user,
        firstname='Rutvik',
        last_name='Rajyaguru',
        email='rutvik123@gmail.com',
        mobile_no='+919856325410',
        city='Rajula',
        state='Gujarat',
        Zip=365560,
        residential_address='3,safd,afdsg,Ahmedabad',
    )
    addresses.save()
    return addresses


@pytest.fixture
def order_placed(user, product, address):
    op = OrderPlaced.objects.create(
        user=user,
        product=product,
        quantity=1,
        address=address,
        ordered_date='2022-08-07',
        mode='online',
        status='Pending',
        is_canceled=False,
        total_amount=220.0,
    )
    op.save()
    return op


@pytest.fixture
def applied_coupon(coupon_code, user):
    coupon = AppliedCouponCode.objects.create(
        coupon=coupon_code,
        user=user,
    )
    coupon.save()
    return coupon


@pytest.fixture
def customer(new_user):
    new_customer = Customer.objects.create(
        user_id=new_user.id,
        email='rutvikraj9999@gmail.com',
        firstname='Rutvik',
        last_name='Rajyaguru',
        mobile_no='+919856547852',
        city='Rajula',
        state='Gujarat',
        Zip=365560,
        Address='3,safd,afdsg,Ahmedabad',
        profile_pic='default.jpg',
    )
    new_customer.save()
    return new_customer
