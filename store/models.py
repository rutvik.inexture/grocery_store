from django.db import models
from django.contrib.auth.models import User
from cloudinary.models import CloudinaryField
from User.models import Addres


class Category(models.Model):
    """
    Table for Categories of Available Product
    """
    category = models.CharField(max_length=100)

    def __str__(self):
        return str(self.category)


class Product(models.Model):
    """
    Table for Products
    """
    title = models.CharField(max_length=100)
    selling_price = models.FloatField()
    discounted_price = models.FloatField()
    description = models.CharField(max_length=500)
    stock = models.IntegerField()
    is_popular = models.BooleanField(default=False)
    category = models.CharField(choices=((i.category, i.category) for i in Category.objects.all()), max_length=50)
    product_img = CloudinaryField('product_img')

    def __str__(self):
        return str(self.id)


class Cart(models.Model):
    """
    Table for Cart (all products added to cart will be stored here)
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(default=1)
    ordered = models.BooleanField(default=False)

    def __str__(self):
        return str(self.user)

    @property
    def total_cost(self):
        return self.quantity * self.product.discounted_price


# review table for products
class Review(models.Model):
    """
    Table for reviews of Products
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    review = models.TextField()

    def __str__(self):
        return str(self.user)


status_choice = (
    ('Accepted', 'Accepted'),
    ('Packed', 'Packed'),
    ('Out For Delivery', 'Out For Delivery'),
    ('Delivered', 'Delivered'),
    ('Cancel', 'Cancel'),
)

payment_choices = (
    ('Online', 'Online'),
    ('COD', 'COD'),
)


class OrderPlaced(models.Model):
    """
    Order Placed table (All the confirmed orders will be stored)
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(default=1)
    address = models.ForeignKey(Addres, on_delete=models.CASCADE)
    ordered_date = models.DateField(auto_now_add=True)
    mode = models.CharField(max_length=200, choices=payment_choices, default='COD')
    status = models.CharField(max_length=200, choices=status_choice, default='Pending')
    ordered = models.BooleanField(default=True)
    is_canceled = models.BooleanField(default=False)
    total_amount = models.FloatField(default=0.00)


class CouponCode(models.Model):

    code = models.CharField(max_length=150)
    valid_from = models.DateTimeField()
    valid_to = models.DateTimeField()
    discount = models.IntegerField()
    active = models.BooleanField()
    condition = models.CharField(max_length=200, null=True)

    def __str__(self):
        return str(self.code)


class AppliedCouponCode(models.Model):
    coupon = models.ForeignKey(CouponCode, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.coupon.code)
