# Generated by Django 4.0.5 on 2022-07-12 09:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0024_couponcode'),
    ]

    operations = [
        migrations.AlterField(
            model_name='couponcode',
            name='condition',
            field=models.CharField(default=None, max_length=200),
        ),
    ]
