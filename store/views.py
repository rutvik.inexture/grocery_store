import json
import os
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils.http import urlencode
from django.views import View
from django.views.generic import ListView
from datetime import datetime
from .models import Category, Product, Cart, Addres, Review, OrderPlaced, CouponCode, AppliedCouponCode
from django.contrib import messages
from django.core.paginator import Paginator
import stripe

stripe.api_key = os.getenv('STRIPE_SECRET_KEY')


class CategoryView(View):
    def get(self, request, pk, data=None):
        """
        Category View for drop down
        """
        category = Category.objects.all()
        category_item = Category.objects.get(pk=pk)
        products = Product.objects.all().order_by('category')
        if not data:
            category_item = Category.objects.get(pk=pk)
            products = Product.objects.filter(category=category_item)
            p = Paginator(products, 4)
            page = request.GET.get('page')
            product_item = p.get_page(page)
        elif data == 'price-low-to-high':
            category_item = Category.objects.get(pk=pk)
            product_low_to_high = Product.objects.filter(category=category_item).order_by('discounted_price')
            paginator = Paginator(product_low_to_high, 4)
            page = request.GET.get('page')
            product_low_to_high_obj = paginator.get_page(page)
            return render(request, 'store/category.html',
                          {'category_item': category_item, 'category': category,
                           'product_low_to_high': product_low_to_high_obj})
        elif data == 'price-high-to-low':
            category_item = Category.objects.get(pk=pk)
            product_high_to_low = Product.objects.filter(category=category_item).order_by('-discounted_price')
            paginator = Paginator(product_high_to_low, 4)
            page = request.GET.get('page')
            product_high_to_low_obj = paginator.get_page(page)
            return render(request, 'store/category.html',
                          {'category_item': category_item, 'category': category,
                           'product_high_to_low': product_high_to_low_obj})
        elif data == 'popular-products':
            category_item = Category.objects.get(pk=pk)
            popular_products = Product.objects.filter(category=category_item).order_by('is_popular')
            paginator = Paginator(popular_products, 4)
            page = request.GET.get('page')
            popular_products_obj = paginator.get_page(page)
            return render(request, 'store/category.html',
                          {'category_item': category_item, 'category': category,
                           'popular_products': popular_products_obj})

        return render(request, 'store/category.html',
                      {'category_item': category_item, 'products': products, 'category': category,
                       'product_item': product_item})


class FilterCategoryView(View):
    def get(self, request, pk):
        category = Category.objects.all()
        category_item = Category.objects.get(pk=pk)
        min_value = self.request.GET.get('min_value')
        max_value = self.request.GET.get('max_value')
        product_filter = Product.objects.filter(discounted_price__gte=min_value, discounted_price__lte=max_value,
                                                category=category_item)
        paginator = Paginator(product_filter, 4)
        page = request.GET.get('page')
        page_obj = paginator.get_page(page)

        return render(request, 'store/category_filter.html',
                      {'category_item': category_item, 'product_filter': product_filter,
                       'page_obj': page_obj, 'min_value': min_value, 'max_value': max_value})


class ProductView(View):
    def get(self, request):
        """
        Method for Slider of Products in HomePage
        """
        category = Category.objects.all()
        products = Product.objects.filter(is_popular=True).order_by('category')
        return render(request, 'store/home.html', {'category': category, 'products': products})


class ProductDetailView(View):
    def get(self, request, pk):
        """
        Method for Details view of Particular Product
        """
        user = request.user
        product = Product.objects.get(pk=pk)
        category = Category.objects.all()
        product_review = Review.objects.all()
        if user.is_authenticated:
            item_already_in_cart = Cart.objects.filter(product=product.id, user=request.user, ordered=False)
            return render(request, 'store/product_detail.html',
                          {'product': product, 'item_already_in_cart': item_already_in_cart, 'category': category,
                           'product_review': product_review})
        else:
            return render(request, 'store/product_detail.html',
                          {'product': product, 'category': category, 'product_review': product_review})


class AddToCartView(View):
    def get(self, request):
        """
        Method for adding Products in Cart
        """
        user = request.user
        product_id = request.GET.get('prod_id')
        product = Product.objects.get(id=product_id)
        Cart(user=user, product=product, ordered=False).save()
        return redirect(reverse('cart'))


class BuyNowView(View):
    def get(self, request):
        user = request.user
        product_id = request.GET.get('prod_id')
        product = Product.objects.get(id=product_id)
        Cart(user=user, product=product, ordered=False).save()
        return redirect('/checkout')


class CartView(View):
    def get(self, request):
        """
        Method for Showing all the items in Cart
        """
        if request.user.is_authenticated:
            user = request.user
            category = Category.objects.all()
            cart = Cart.objects.filter(user=user)
            all_available_coupon = CouponCode.objects.all()
            for c in cart:
                if not c.ordered:
                    amount = 0.0
                    shipping_amount = 0.0
                    total_amount = 0.0
                    cart_product = Cart.objects.filter(user=user, ordered=False)
                    if cart_product:
                        for p in cart_product:
                            tempamount = (p.quantity * p.product.discounted_price)
                            amount += tempamount
                            if amount > 10.0 and amount < 200.0:
                                shipping_amount = 100.0
                            elif amount > 200.0 and amount < 1000.0:
                                shipping_amount = 70.0
                            elif amount > 1000.0:
                                shipping_amount = 0.0
                        total_amount = amount + shipping_amount
                        return render(request, 'store/addtocart.html',
                                      {'user': user, 'carts': cart, 'total_amount': total_amount, 'amount': amount,
                                       'category': category, 'shipping_amount': shipping_amount,
                                       'all_available_coupon': all_available_coupon})

            return render(request, 'store/emptycart.html')

    def post(self, request):
        """
        Post method for applying coupon code and validating the codes
        """
        code = request.POST.get('coupon_code')
        available_coupon = CouponCode.objects.get(code=code)
        total_amount = float(request.POST.get('total_amount'))
        condition = available_coupon.condition.split('-')
        user = request.user
        is_condition = available_coupon.condition
        all_available_coupon = CouponCode.objects.all()
        category = Category.objects.all()
        cart = Cart.objects.filter(user=user)
        is_applied = AppliedCouponCode.objects.filter(user=request.user, coupon=available_coupon)
        for c in cart:
            if not c.ordered:
                amount = 0.0
                shipping_amount = 0.0
                cart_product = Cart.objects.filter(user=user, ordered=False)
                if cart_product:
                    for p in cart_product:
                        tempamount = (p.quantity * p.product.discounted_price)
                        amount += tempamount
                        if amount > 10.0 and amount < 200.0:
                            shipping_amount = 100.0
                        elif amount > 200.0 and amount < 1000.0:
                            shipping_amount = 70.0
                        elif amount > 1000.0:
                            shipping_amount = 0.0
                    current_time = datetime.now()
                    discount_percentage = int(available_coupon.discount)
                    valid_from = datetime.strptime(str(available_coupon.valid_from)[:19], "%Y-%m-%d %H:%M:%S")
                    valid_to = datetime.strptime(str(available_coupon.valid_to)[:19], "%Y-%m-%d %H:%M:%S")
                    active = available_coupon.active
                    if not is_applied:
                        if valid_from <= current_time and valid_to >= current_time and active == True and is_condition == 'None':
                            dicount = total_amount * (discount_percentage / 100)
                            total_amount -= dicount
                            messages.success(request, 'Coupoun Code Applied !!!')
                            return render(request, 'store/addtocart.html',
                                          {'user': user, 'carts': cart, 'total_amount': total_amount, 'amount': amount,
                                           'category': category, 'shipping_amount': shipping_amount,
                                           'available_coupon': available_coupon,
                                           'all_available_coupon': all_available_coupon})

                        elif valid_from <= current_time and valid_to >= current_time and active == True and is_condition != 'None':
                            min_val = int(condition[0])
                            max_val = int(condition[1])
                            if total_amount >= min_val and total_amount <= max_val:
                                discount = total_amount * (discount_percentage / 100)
                                total_amount -= discount
                                messages.success(request, 'Coupoun Code Applied !!!')
                                return render(request, 'store/addtocart.html',
                                              {'user': user, 'carts': cart, 'total_amount': total_amount,
                                               'amount': amount,
                                               'category': category, 'shipping_amount': shipping_amount,
                                               'available_coupon': available_coupon,
                                               'all_available_coupon': all_available_coupon})
                            else:
                                messages.warning(request,
                                                 f'Coupon Code is invalid (Applicable for orders between {min_val}-{max_val})')
                        else:
                            messages.warning(request, f'Coupon Code Expired')
                    else:
                        messages.warning(request, f'Coupon is Already Used')

                return redirect('cart')
        return render(request, 'store/emptycart.html')


class UpdateCartView(View):
    def post(self, request):
        """
        Method for Updating Quantity of Products in Cart
        """
        prod_id = int(request.POST.get('product_id'))
        amount = 0.0
        shipping_amount = 0.0
        total_amount = 0.0
        if Cart.objects.filter(product_id=prod_id, user=request.user):
            prod_qty = request.POST.get('product_qty')
            cart = Cart.objects.filter(product_id=prod_id, user=request.user).first()
            cart.quantity = prod_qty
            Cart.objects.filter(product_id=prod_id, user=request.user).update(quantity=prod_qty)
            user = request.user
            cart_product = [p for p in Cart.objects.all() if p.user == user]
            if cart_product:
                for p in cart_product:
                    tempamount = (p.quantity * p.product.discounted_price)
                    amount += tempamount
                    if amount > 10.0 and amount < 200.0:
                        shipping_amount = 100.0
                    elif amount > 200.0 and amount < 500.0:
                        shipping_amount = 70.0
                    elif amount > 1000.0:
                        shipping_amount = 0.0
                data = {
                    'amount': amount,
                    'totalamount': amount + shipping_amount
                }
                return JsonResponse(data)


class RemoveCartView(View):
    def post(self, request):
        """
        Method for removing item from the cart
        """
        prod_id = int(request.POST.get('product_id'))
        amount = 0.0
        shipping_amount = 0.0
        total_amount = 0.0
        if (Cart.objects.filter(product_id=prod_id, user=request.user)):
            cartitem = Cart.objects.get(product_id=prod_id, user=request.user)
            cartitem.delete()
            user = request.user
            cart_product = [p for p in Cart.objects.all() if p.user == user]
            for p in cart_product:
                tempamount = (p.quantity * p.product.discounted_price)
                amount += tempamount
                if amount > 100.0 and amount < 200.0:
                    shipping_amount = 100.0
                elif amount > 200.0 and amount < 500.0:
                    shipping_amount = 70.0
                elif amount > 1000.0:
                    shipping_amount = 0.0
            data = {
                'amount': amount,
                'totalamount': amount + shipping_amount
            }
            return JsonResponse(data)
        return redirect('/')


class ReviewView(View):

    def post(self, request):
        """
        Method for comments/Review of Particular
        """
        user = request.user
        product_id = request.POST['product_id']
        review = request.POST['review']
        Review(user=user, product_id=product_id, review=review).save()
        return JsonResponse({'status': 'Save'})


class ReviewDeleteView(View):
    def post(self, request):
        """
        Method to Delete posted Review for the product
        """
        review_id = request.POST['review_id']
        Review(id=review_id).delete()
        return JsonResponse({'status': 'Save'})


class SearchProductView(View):
    """
    Method for fetching all the products for jqueryautocomplete
    """

    def get(self, request):
        products = Product.objects.values_list('title', flat=True)
        product_list = list(products)

        return JsonResponse(product_list, safe=False)


class SearchedItemView(View):
    def get(self, request):
        return redirect('/')

    def post(self, request):
        """
        View for fetching input value and pass it for JsonResponse (ajax)
        """
        searched_items = request.POST.get('searchproducts')
        if searched_items == "":
            return redirect('/')
        else:
            product = Product.objects.filter(title__contains=searched_items).first()
            if product:
                return render(request, 'store/search.html', {'product': product})
            else:
                messages.info(request, "Sorry No Matching Product Found !!! ")
                return redirect('/')


class CheckoutView(View):
    def get(self, request):
        """
        Checkout View for Cart Products
        """
        user = request.user
        add = Addres.objects.filter(user=user)
        total_amount = request.GET.get('total_amount')
        shipping_amount = request.GET.get('shipping_amount')
        cart_item = Cart.objects.filter(user=user, ordered=False)
        cart_item_id = [i.product.id for i in cart_item]
        return render(request, 'store/checkout.html',
                      {'add': add, 'total_amount': total_amount,
                       'shipping_amount': shipping_amount, 'cart_item': cart_item, 'cart_item_id': cart_item_id})


class OrderPlacedView(View):
    def post(self, request):
        """
        Method for placing order
        """
        user = request.user
        address_item = request.POST.get('address_id')
        product_id = request.POST.get('prod_id')
        product_stock = int(request.POST.get('prod_stock'))
        order_quantity = int(request.POST.get('order_qty'))
        address = Addres.objects.get(id=address_item)
        total_amount = request.POST.get('total_amount')
        cart = Cart.objects.filter(user=user, ordered=False)
        for c in cart:
            stock = product_stock - order_quantity
            OrderPlaced(user=user, product=c.product, quantity=c.quantity, address=address,
                        total_amount=total_amount).save()
            Product.objects.filter(id=product_id).update(stock=stock)
            Cart.objects.filter(id=c.id).update(ordered=True)
        coupon_code = request.POST.get('code')
        print(type(coupon_code))
        if coupon_code != "":
            applied_coupon = CouponCode.objects.get(code=coupon_code)
            AppliedCouponCode(coupon=applied_coupon, user=user).save()
        return redirect('orders')


class OrdersView(View):
    def get(self, request):
        """
        Method for Viewing orders
        """
        order_placed = OrderPlaced.objects.filter(user=request.user, ordered=True, is_canceled=False)
        category = Category.objects.all()
        if not order_placed:
            messages.success(request, f'No Pending Orders')
            return redirect('/')
        return render(request, 'store/orders.html', {'order_placed': order_placed, 'category': category})


class CancelOrderView(View):
    def post(self, request):
        """
        Method for canceling order until admin accepts the order
        """
        order_id = int(request.POST['order_id'])
        product_id = int(request.POST.get('prod_id'))
        product_stock = int(request.POST.get('prod_stock'))
        order_qty = int(request.POST.get('order_qty', False))
        stock = product_stock + order_qty
        Product.objects.filter(id=product_id).update(stock=stock)
        OrderPlaced.objects.filter(id=order_id).update(ordered=False, is_canceled=True)
        return JsonResponse({'status': 'Save'})


class OrderHistoryView(View):
    def get(self, request):
        """
        Method for viewing history of orders
        """
        order_placed = OrderPlaced.objects.filter(user=request.user)
        cart = Cart.objects.filter(user=request.user)
        product = Product.objects.all()
        product_review = Review.objects.filter(user=request.user)
        category = Category.objects.all()
        if order_placed:
            return render(request, 'store/orderhistory.html',
                          {'order_placed': order_placed, 'product': product, 'product_review': product_review,
                           'category': category})
        else:
            messages.success(request, f'No orders Yet')
            return redirect('orders')


class CreateCheckoutSessionView(View):
    def post(self, request):
        """
        Creating a checkout session for online Payments
        """
        host = self.request.get_host()
        user = request.user
        address_item = request.POST.get('address_id')
        total_amount = float(request.POST.get('total_amount'))
        # total_amount = float(total_amount)
        shipping_amount = request.POST.get('shipping_amount')
        address = Addres.objects.get(id=address_item)
        cart = Cart.objects.filter(user=request.user, ordered=False)
        product_ids = list(map(int, request.POST.getlist('prod_id')))
        product_stock = list(map(int, request.POST.getlist('prod_stock')))
        order_quantity = list(map(int, request.POST.getlist('order_qty')))
        order_ids = []
        mode = "Online"
        for c in cart:
            order_obj = OrderPlaced.objects.create(user=user, product=c.product, quantity=c.quantity, address=address,
                                                   mode=mode, total_amount=total_amount)
            order_obj.save()
            order_ids.append(order_obj.id)
            for a, b, c in zip(product_ids, product_stock, order_quantity):
                Product.objects.filter(id=int(a)).update(stock=(int(b) - int(c)))
        cart_product = Cart.objects.filter(user=user, ordered=False)
        if cart_product:
            for p in cart_product:
                product_title = p.product.title
        placed_od = total_amount
        title = product_title
        checkout_session = stripe.checkout.Session.create(
            payment_method_types=['card'],
            line_items=[
                {
                    'price_data': {
                        'currency': 'inr',
                        'unit_amount': int((placed_od) * 100),
                        'product_data': {
                            'name': title,
                        },
                    },
                    'quantity': 1,
                },
            ],
            mode='payment',
            success_url="http://{}{}".format(host, reverse('orders')),
            cancel_url="http://{}{}".format(host,
                                            f"{reverse('cancelorder')}?{urlencode({'product_ids': product_ids, 'product_stock': product_stock, 'order_quantity': order_quantity, 'order_ids': order_ids})}"),
        )
        for c in cart:
            Cart.objects.filter(id=c.id).update(ordered=True)
            OrderPlaced.objects.filter(user=user, address=address).update(ordered=True)
        coupon_code = request.POST.get('code')
        if coupon_code != '':
            applied_coupon = CouponCode.objects.get(code=coupon_code)
            AppliedCouponCode(coupon=applied_coupon, user=user).save()
        return redirect(checkout_session.url, code=303)


class CancelOrderCheckoutView(View):

    def get(self, request):
        """
        Cancel order if there are any blockers during online payments
        """
        user = request.user
        order_id = request.GET.get('order_ids')
        order_id = json.loads(order_id)
        product_id = request.GET.get('product_ids')
        product_id = json.loads(product_id)
        product_stock = request.GET.get('product_stock')
        product_stock = json.loads(product_stock)
        order_quantity = request.GET.get('order_quantity')
        order_quantity = json.loads(order_quantity)
        cart = Cart.objects.filter(user=user)
        for a, b, c in zip(product_id, product_stock, order_quantity):
            Product.objects.filter(id=a).update(stock=b + c)
        for i in order_id:
            OrderPlaced.objects.filter(id=i).update(ordered=False, is_canceled=True)
        for c in cart:
            Cart.objects.filter(id=c.id).update(ordered=False)
        messages.warning(request, f"Order has been Cancelled !!")
        return redirect('/')
