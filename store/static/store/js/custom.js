$('#slider1, #slider2, #slider3').owlCarousel({
    loop: true,
    margin: 20,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: false,
            autoplay: true,
        },
        600: {
            items: 3,
            nav: true,
            autoplay: true,
        },
        1000: {
            items: 4,
            nav: true,
            loop: true,
            autoplay: true,
        }
    }
})


var availableTags = [];
$.ajax({
    method: "GET",
    url : "/product-list",
    success: function (response){
//        console.log(response)
        startAutoComplete(response);
    }
});
function startAutoComplete(availableTags)
{
    $( "#searchproducts" ).autocomplete({
    source: availableTags
    });
}

$(document).ready(function (){

    $('.increment-button').click(function (e){
        e.preventDefault();
        var inc_value = $(this).closest('.product_data').find('.qty-input').val();
        var value = parseInt(inc_value,10);
        var stk = $(this).closest('.product_data').find('.prod_stock').val();
        console.log(stk)
        console.log(value)
        value = isNaN(value) ? 0: (value);
        if(value < stk)
        {
            value++;
            $(this).closest('.product_data').find('.qty-input').val(value);
            console.log(value)
        }


    });


});

$(document).ready(function (){
    $('.decrement-button').click(function (e){
        e.preventDefault();
        var inc_value = $(this).closest('.product_data').find('.qty-input').val();
        var value = parseInt(inc_value,10);
        value = isNaN(value) ? 0: (value);
        if(value > 1)
        {
            value--;
            $(this).closest('.product_data').find('.qty-input').val(value);
        }
    });
});



$(document).ready(function (){
    $('.review-btn').click(function (e){
        e.preventDefault();
        console.log("Button Clicked", e);
        var product_id = $(this).closest('.prod_data').find('.prod_id').val();
        var token = $('input[name=csrfmiddlewaretoken]').val();
        console.log(token)
        let rw = $(".review-item").val()
        if (rw == ""){
            console.log('Please Enter Review')
        } else {
            console.log(rw)
            console.log(product_id)
            mydata = {product_id:product_id, review:rw,csrfmiddlewaretoken : token};
            $.ajax({
                url : "/add-review/",
                method : "POST",
                data : mydata,
                success:function(data){
                    console.log(data);
                    if (data.status == "Save"){
                        console.log('Commented Succesfully');
                        location.reload();
                        $("#reviewform")[0].reset();

                    }

                }
            })
        }
    });


    $('.changeQuantity').click(function (e){
        e.preventDefault();
        var product_id = $(this).closest('.prod_data').find('.prod_id').val();
        var product_qty = $(this).closest('.prod_data').find('.qty-input').val();
        var token = $('input[name=csrfmiddlewaretoken]').val();
        $.ajax({
            method: "POST",
            url : "/update-cart/",
            data : {
                'product_id':product_id,
                'product_qty':product_qty,
                csrfmiddlewaretoken : token

            },
            success: function (data) {
                console.log("Changed")
                document.getElementById("amount").innerText= data.amount
                document.getElementById("totalamount").innerText= data.totalamount
                console.log(data.totalamount)
            }

        });
    });


    $('.remove-cart').click(function (e){
        e.preventDefault();
        var product_id = $(this).closest('.prod_data').find('.prod_id').val();
        var token = $('input[name=csrfmiddlewaretoken]').val();
        $.ajax({
            method: "POST",
            url : "/remove-cart/",
            data : {
                'product_id':product_id,
                csrfmiddlewaretoken : token

            },
            success: function (data) {
                $('.cartdata').load(location.href + " .cartdata")
                document.getElementById("amount").innerText= data.amount
                document.getElementById("totalamount").innerText= data.totalamount
                location.reload();
                console.log(data)

            }

        });
    });

    $('.review-delete-btn').click(function (e){
        e.preventDefault();
        console.log("Button Clicked", e);
        var product_id = $(this).closest('.prod_data').find('.prod_id').val();
        var token = $('input[name=csrfmiddlewaretoken]').val();
        console.log(token)
        let rw = $(".user_review").val()
        let rw_id = $(".review_id").val()
         {
            console.log(rw)
            console.log(product_id)
            mydata = {product_id:product_id, review:rw, review_id:rw_id, csrfmiddlewaretoken : token};
            $.ajax({
                url : "/review-delete/",
                method : "POST",
                data : mydata,
                success:function(data){
                    if (data.status == "Save"){
                        console.log('Deleted Succesfully');
                        location.reload();
                    }
                }
            })
        }
    });

    $('.address-delete-btn').click(function (e){
        e.preventDefault();
        console.log("Button Clicked", e);
        var address_id = $(this).closest('.add_data').find('.address_id').val();
        console.log(address_id)
        var token = $('input[name=csrfmiddlewaretoken]').val();
         {
            mydata = {address_id:address_id, csrfmiddlewaretoken : token};
            $.ajax({
                url : "/addressdelete/",
                method : "POST",
                data : mydata,
                success:function(data){
                    if (data.status == "Save"){
                        console.log('Deleted Succesfully');
                        location.reload();
                    }
                }
            })
        }
    });


    $('.cancel-order-btn').click(function (e){
        e.preventDefault();
        console.log("Button Clicked", e);
        var order_id = $(this).closest('.cancel_data').find('.order_id').val();
        var prod_id = $(this).closest('.cancel_data').find('.prod_id').val();
        var prod_stock = $(this).closest('.cancel_data').find('.prod_stock').val();
        var order_qty = $(this).closest('.cancel_data').find('.order_qty').val();
        console.log(order_id)
        console.log(prod_id)
        console.log(order_qty)

        var token = $('input[name=csrfmiddlewaretoken]').val();
         {
            mydata = {order_id:order_id, prod_id:prod_id, prod_stock:prod_stock, order_qty:order_qty, csrfmiddlewaretoken : token};
            $.ajax({
                url : "/cancel-order/",
                method : "POST",
                data : mydata,
                success:function(data){
                    if (data.status == "Save"){
                        console.log('Cancelled Succesfully');
                        console.log(order_id)
                        console.log(prod_id)
                        console.log(order_qty)
                        location.reload();
                    }
                }
            })
        }
    });


    $('.order-confirmed-btn').click(function (e){
        e.preventDefault();
        var prod_id = $("[name='prod_id']").val();
        var prod_stock = $("[name='prod_stock']").val();
        var order_qty = $("[name='order_qty']").val();
        var address_id = $("[name='address_id']").val();
        console.log(prod_id)
        console.log(prod_stock)
        console.log(order_qty)
        console.log(address_id)
        console.log("Button Clicked", e);
        var token = $('input[name=csrfmiddlewaretoken]').val();
         {
            mydata = {prod_id:prod_id, prod_stock:prod_stock, order_qty:order_qty, csrfmiddlewaretoken : token};
            $.ajax({
                url : "/orderconfirm/",
                method : "POST",
                data : mydata,
                success:function(data){
                    if (data.status == "Save"){
                        console.log('Cancelled Succesfully');
                        console.log(prod_id)
                        console.log(order_qty)
                        console.log(address_id)
                        window.location.href = "/place-order"
//                        location.reload();
                    }
                }
            })
        }
    });


});






