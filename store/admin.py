from django.contrib import admin
from .models import Category, Product, Cart, Review, OrderPlaced, CouponCode, AppliedCouponCode


@admin.register(Product)
class ProductModelAdmin(admin.ModelAdmin):
    """
    Product table registration
    """
    list_display = ['id', 'title', 'selling_price', 'discounted_price', 'description', 'stock', 'category',
                    'product_img', 'is_popular']


@admin.register(Category)
class CategoryModelAdmin(admin.ModelAdmin):
    """
    Category table Registration
    """
    list_display = ['id', 'category']


@admin.register(Cart)
class CartModelAdmin(admin.ModelAdmin):
    """
    Cart Table Registration
    """
    list_display = ['id', 'user', 'product', 'quantity', 'ordered']


@admin.register(Review)
class ReviewModelAdmin(admin.ModelAdmin):
    """
    Review Table Registration
    """
    list_display = ['id', 'user', 'product', 'review']


@admin.register(OrderPlaced)
class OrderPlacedAdmin(admin.ModelAdmin):
    """
    Order Placed Details
    """
    list_display = ['id', 'user', 'product', 'quantity', 'address', 'ordered_date', 'status', 'mode', 'ordered', 'is_canceled', 'total_amount']


@admin.register(CouponCode)
class CouponCodeAdmin(admin.ModelAdmin):
    list_display = ['id', 'code', 'valid_from', 'valid_to', 'discount', 'active', 'condition']


@admin.register(AppliedCouponCode)
class AppliedCouponCodeAdmin(admin.ModelAdmin):
    list_display = ['id', 'coupon', 'user']
