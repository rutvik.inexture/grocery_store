from django.urls import path
from store import views
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('', views.ProductView.as_view(), name='homepage'),
    path('product-detail/<int:pk>', views.ProductDetailView.as_view(), name='product-detail'),
    path('cart/', login_required(views.CartView.as_view()), name='cart'),
    path('product-list/', views.SearchProductView.as_view(), name='search'),
    path('search/', views.SearchedItemView.as_view(), name='searcheditem'),
    path('add-to-cart/', views.AddToCartView.as_view(), name='addtocart'),
    path('buy-now/', views.BuyNowView.as_view(), name='buy-now'),
    path('add-review/', views.ReviewView.as_view(), name='review'),
    path('review-delete/', views.ReviewDeleteView.as_view(), name='reviewdelete'),
    path('update-cart/', views.UpdateCartView.as_view(), name='updatecart'),
    path('remove-cart/', views.RemoveCartView.as_view(), name='removecart'),
    path('category/<int:pk>', views.CategoryView.as_view(), name='category'),
    path('category-filter/<int:pk>', views.FilterCategoryView.as_view(), name='category-filter'),
    path('category/<int:pk>/<slug:data>', views.CategoryView.as_view(), name='category-data'),
    path('checkout/', login_required(views.CheckoutView.as_view()), name='checkout'),
    path('place-order/', login_required(views.OrderPlacedView.as_view()), name='Place-order'),
    path('orders/', login_required(views.OrdersView.as_view()), name='orders'),
    path('order-history/', login_required(views.OrderHistoryView.as_view()), name='order-history'),
    path('cancel-order/', login_required(views.CancelOrderView.as_view()), name='cancel-order'),
    path('create-checkout-session/', login_required(views.CreateCheckoutSessionView.as_view()),
         name='create-checkout-session'),
    path('cancelorder/', views.CancelOrderCheckoutView.as_view(), name='cancelorder'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
